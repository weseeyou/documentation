import tensorflow as tf


print("")
print("IS GPU AVAILABLE")
if (tf.test.is_gpu_available()):
    print("GPU available")
else:
    print("GPU not available")

print("")
print("LIST LOCAL DEVICES GPU")
if (len(tf.config.list_physical_devices('GPU')) > 0):
    print("GPU available")
else:
    print("GPU not available")
