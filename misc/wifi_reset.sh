## copy to /usr/local/bin/
## sudo chmod 775 /usr/local/bin/wifi_reset.sh
## add this to crontab of root
## sudo crontab -e
## eg: */5 * * * * /usr/bin/sudo -H /usr/local/bin/wifi_reset.sh >> /dev/null 2>&1

ping -c4 192.168.178.1 > /dev/null
 
if [ $? != 0 ] 
then
  echo "No network connection, restarting wlan0"
  /sbin/ifdown 'wlan0'
  sleep 5
  /sbin/ifup --force 'wlan0'
fi