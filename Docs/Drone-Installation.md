# Drone installation instructions
## General Information
The Drone component provides: 

- an API (Flask) for the AIMachine to check if the drone is still alive
- an API (Flask) for the AIMachine to deliver the transformed images to
- a service to register itself with the AIMachine
- an image player that listens for incoming images to display

The register drone with aimachine sequence

![A sequence visualisation](../Diagrams/TT_DroneRegister.seq.png)

The aimachine check drone sequence

![A sequence visualisation](../Diagrams/TT_AIMachineCheckDrones.seq.png)

The drone receive image sequence

![A sequence visualisation](../Diagrams/TT_DroneReceiveImage.seq.png)

## Raspbian Installation
First follow the raspbian [installation guide](./Raspbian.md#raspberry-pi-with-raspbian).

## Host file entries
Because there is no DNS server present for this installation. Put the correct entries in the [hosts file](./hosts.md#drone-hosts-file).

# Docker
Since version 1.1 of the drone application is running inside a docker container. First install docker.

## Install docker
Install the raspberry pi docker version via 

    curl -sSL https://get.docker.com | sh

Add the pi user to the the docker group 

    sudo usermod -aG docker pi

Start docker with 

    sudo systemctl start docker

## Docker compose
To run the application use the docker-compose file in the docker project. 
First install docker-compose

    sudo apt-get install docker-compose

## Clone the docker project
Clone the docker project into the dev directory

    mkdir dev
    cd dev

    git clone https://gitlab.com/weseeyou/docker.git

Depending on the drone architecture startup the application with the following:

Use 

    cat /proc/cpuinfo

To get the correct processor architecture.

Then for armv32v7 use:
    
    docker-compose -f ~/dev/drone/arm32v7/docker-compose.<prod|dev>.yml up -d

And for armv32v6 use:

    docker-compose -f ~/dev/drone/arm32v6/docker-compose.<prod|dev>.yml up -d

## Fix some wifi problems disconnects (OPTIONAL)
I have experienced some wifi instability on the raspberry pi's. To remedy this a couple of hacks have been applied to try to remedy this. 

- disable power management on the wifi adapter

        sudo vi /etc/rc.local
        add 
            /sbin/iwconfig wlan0 power off
        before exit 0
- up/down the wifi interface every 5 minutes

        create the file /usr/local/bin/wifi_reset.sh

with this content

        ping -c4 192.168.178.1 > /dev/null
 
        if [ $? != 0 ] 
        then
            echo "No network connection, restarting wlan0"
            /sbin/ifdown 'wlan0'
            sleep 5
            /sbin/ifup --force 'wlan0'
        fi

and then make executable.

        sudo chmod +x /usr/local/bin/wifi_reset.sh
        sudo chmod 775 /usr/local/bin/wifi_reset.sh
        
add this line to crontab of root to schedule it every 5 minutes
            
        sudo crontab -e
        
        */5 * * * * /usr/bin/sudo -H /usr/local/bin/wifi_reset.sh >> /dev/null 2>&1

## Scheduled reboot (OPTIONAL)
It could help network stability to reboot the drones on a regular interval. (Dronserver start will register with the AImachine). To do that enter a reboot command in the crontab (root).

    sudo crontab -e

add the line 

    0 0 * * * /sbin/shutdown -r

## Get the source (obsolete)
    git clone https://gitlab.com/spawnwvinull/weseeyou.git

## Frame Buffer Image View (FBI) (obsolete)
To display the images on the drone I have chosen the simple fbi application. Just installation no configuration is needed.

    sudo apt-get install fbi

## Properly enable numpy (obsolete)
To get numpy to work properly install these additional libraries.

    sudo apt-get install python-dev libatlas-base-dev

## Enable the network-online service (obsolete)
The services need to wait for the network to be online before they can be started. (The first thing a drone does is register with the AIMachine). The wait online service needs to be enabled first.

    sudo systemctl enable systemd-networkd-wait-online



## The services (obsolete)
### Setup the virtual environment
For now (march 2020) the python code is installed and run in a virtual environment. Configure virtual env for the AI machine and for the Drones.

- First upgrade pip 

        python3 -m pip install --upgrade pip

- Then install virtual env (if needed)

        python3 -m pip install virtualenv

- Then create/setup the virtual environment
    
        cd ~/weseeyou
        python3 -m venv env
    
- and activate it with 
    
        source env/bin/activate

- After activation of the virtual enviroment, upgrade pip again but now for this virtual environment.
    
        python3 -m pip install --upgrade pip

- Also upgrade the setuptools 
    
        pip install setuptools==41.0.0

- Then within the virtual env install the packages with 

        cd ~/weseeyou/Drone
        pip install -r requirements.drone.txt
    
### Start the services (obsolete)
After all python packages are successfully installed we can start the services. The drone has two services:
1.  the flask service to serve the api requests aka the <b>droneserver</b>

Start this service with:
    
    cd ~/weseeyou/Drone
    python wsgi.py -c Config/DroneServer.json

2.  the service that listens for incoming images and displays them with the help of fbi aka <b>droneplayer</b>

Start this service with:

    cd ~/weseeyou/Drone
    python ImagePlayerApp.py -c Config/DronePlayer.json

## The configuration (obsolete)
Both processes are configured via the Config/*.json files.

<b>DroneServer.json</b>
    
    {
        "BASE_DIR": "/home/pi/weseeyou/Drone",
        "APPLICATION_PORT": "5018",
        "WORKING_DIR": "Images",
        "LOG_DIR": "logs",
        "LOG_FILE": "droneserver.log",
        "LOG_LEVEL": "DEBUG",
        "HOST_ADDRESS": "0.0.0.0",
        "AISERVER_BASEURL": "http://twistedfate:5016",
        "AISERVER_REGISTER_DRONE_ENDPOINT": "/api/drone/register",
        "DRONE_SERVER_ENDPOINT": "/api/drone/image",
        "ENVIRONMENT": "Production"
    }

Property | Description | Example
----- |------ |-----
APPLICATION_PORT | The application port for the Flask Service | 5018
WORKING_DIR | The location where the service puts the files for ProcessImages service to listen | "Images" 
BASE_DIR | The application base dir | "/home/spawn/weseeyou/Drone"
LOG_DIR | The application log directory | "logs"
LOG_FILE | The log file name | "droneserver.log"
LOG_LEVEL | The log level | INFO, DEBUG, ERROR
HOST_ADDRESS | Host address definition for the Flask service  | "0.0.0.0"
AISERVER_BASEURL | The aimachine base url | "http://twistedfate:5016"
AISERVER_REGISTER_DRONE_ENDPOINT | The endpoint on the aimachine for drone registration | "/api/drone/image"
ENVIRONMENT | The environment label | PRODUCTION, TEST, DEVELOPMENT

<b>DronePlayer.json</b>

    {
        "BASE_DIR": "/home/pi/weseeyou/Drone",
        "WORKING_DIR": "Images",
        "LOG_DIR": "logs",
        "LOG_FILE": "droneplayer.log",
        "LOG_LEVEL": "DEBUG",
        "STARTUP_IMAGE": "/home/pi/weseeyou/Drone/DisplayImages/startup_image.jpg"
    }

Property | Description | Example
----- |------ |-----
WORKING_DIR | The location where the listens to new images | "Images" 
BASE_DIR | The application base dir | "/home/spawn/weseeyou/Drone"
LOG_DIR | The application log directory | "logs"
LOG_FILE | The log file name | "droneplayer.log"
LOG_LEVEL | The log level | INFO, DEBUG, ERROR
STARTUP_IMAGE | The image path to the startup image. When the drone is started this default image is displayed. | "/home/pi/weseeyou/Drone/DisplayImages/startup_image.jpg"

## Install the services (Raspbian) (obsolete)
The two service can also be integrated in the system and installed as a native service via the following:

Install the services

    sudo cp ~/weseeyou/Drone/Install/* /lib/systemd/system
    sudo chmod 775 /lib/systemd/system/droneserver.service
    sudo chmod 775 /lib/systemd/system/droneplayer.service

Enable the services

    sudo systemctl enable droneserver.service
    sudo systemctl enable droneplayer.service

Start the services:
- droneserver

        sudo systemctl start droneserver.service 
    or

        sudo service droneserver start
 
- droneplayer

        sudo systemctl start droneplayer.service 
    or   

        sudo service droneplayer start

## Logging and monitoring (obsolete)
The services and the process can be monitored in a variety of ways.
- service status commands

        sudo service droneserver status
        sudo service droneplayer status

- via the default syslog of the machine

        tail -f /var/log/syslog
- via the log files. All process log into the configured log directory (default ~/weseeyou/Drone/logs). There you will find:

        droneserver.log
        droneplayer.log

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>

