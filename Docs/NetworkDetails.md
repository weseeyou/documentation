# Network configuration


| Name           | Type            | Mac Address       | IP              | Description                                     |
| -----          |------           |-------------      |----             |-------------                                    |
| WeSeeYou24     | Router          | #                 | 10.66.6.1       | The router to connect the devices               |
| DRONE001       | Raspberry 3B+   | B8:27:EB:E8:C1:A3 | 10.66.6.5       | The drone to receive images for display         |
| DRONE002       | Raspberry Zero  | B8:27:EB:0B:7C:E1 | 10.66.6.6       | The drone to receive images for display         |
| DRONE003       | Raspberry Zero  | B8:27:EB:B9:94:C3 | 10.66.6.8       | The drone to receive images for display         |
| DRONE004       | Raspberry 4     | DC:A6:32:1B:A5:EA | 192.168.178.47  | Test and build agent drone                      |
| DRONE005       | Raspberry 3     |                   | 10.66.6.9       | The drone to receive images for display         |
| DRONE006       | Raspberry Zero  | B8:27:EB:D0:47:08 | 10.66.6.7       | The drone to receive images for display         |
| DRONE007       | Raspberry Zero  | B8:27:EB:33:7F:74 | 10.66.6.12      | The drone to receive images for display         |
| DRONE008       | Raspberry Zero  | B8:27:EB:E5:7E:A5 | 10.66.6.13      | The drone to receive images for display         |
| DRONE009       | Raspberry Zero  | B8:27:EB:78:D3:68 | 10.66.6.14      | The drone to receive images for display         |
| DRONE010       | Raspberry Zero  | B8:27:EB:EA:1D:93 | 10.66.6.15      | The drone to receive images for display         |
| DRONE011       | Raspberry Zero W| B8:27:EB:FE:D4:0B | xx.xx.x.xx      | The drone to receive images for display         |
| WESEEYOU       | Raspberry Zero  | B8:27:EB:40:81:94 | 10.66.6.7       | The eye - motion implementation                 |
| WESEEYOUNOWIFI | Raspberry Zero  | 3C:33:00:5E:70:23 | 10.66.6.4       | The eye - motion implementation                 |  
| WESEEYOUFE     | Raspberry Zero  | B8:27:EB:F2:C3:CA | 10.66.6.10      | The eye (Fisheye) - motion implementation       |
| TWISTEDFATE    | Ubuntu 20.4     | 4C:CC:6A:BC:B3:AE | 10.66.6.3       | The AI machine                                  |
| OTHON          | Windows         | 4C:CC:6A:BB:5F:6F | 192.168.178.29  | The AI machine (DEVELOPMENT and TEST)           |
| AZIR           | Ubuntu 20.4     | 30:9C:23:9B:4C:F2 | 10.66.6.3       | The AI machine (DEVELOPMENT and other projects) |
| INCAL          | Windows         | 58:FB84:95:EB:88  | 10.66.6.2       | The remote management machine                   |


# Network overview
![Network overview](../Diagrams/TT_NetworkOverview.png)

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - may 2021</i></small>