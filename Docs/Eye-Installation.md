# WeSeeYou The Eyes installation instructions

## General Information
The Eye component provides: 
    
- the camera service to capture the images
- the motion service to start capturing when motion is detected
- storage of the images via cif or nfs onto a share on the AIMachine.

## Raspbian Installation
First follow the raspbian [installation guide](./Raspbian.md#raspberry-pi-with-raspbian).

## Get the source
    git clone https://gitlab.com/weseeyou/eye.git

## Fix some wifi problems disconnects (OPTIONAL)
I have experienced some wifi instability on the raspberry pi's. To remedy this a couple of hacks have been applied to try to remedy this. 

- disable power management on the wifi adapter

        sudo vi /etc/rc.local
        add 
            /sbin/iwconfig wlan0 power off
        before exit 0
- up/down the wifi interface every 5 minutes

        sudo cp ~/weseeyou/misc/wifi_reset.sh /usr/local/bin/
        sudo chmod 775 /usr/local/bin/wifi_reset.sh
        
    add this line to crontab of root
            
        sudo crontab -e
        
        */5 * * * * /usr/bin/sudo -H /usr/local/bin/wifi_reset.sh >> /dev/null 2>&1
        
## Scheduled reboot (OPTIONAL)
It could help network stability to reboot the drones on a regular interval. (Dronserver start will register with the AImachine). To do that enter a reboot command in the crontab (root).

    sudo crontab -e

add the line 

    0 0 * * * /sbin/shutdown -r

## Host file entries
Because there is no DNS server present for this installation. Put the correct entries in the [hosts file](./hosts.md#eye-hosts-file).


# Samba (windows CIFS share access)
## Install samba client
    sudo apt-get install samba-common smbclient samba-common-bin smbclient  cifs-utils

## Create motion dir
This is the directory where the motion service will store its images.

    mkdir /home/pi/motion

## Mount the AIMachine share (CIFS SAMBA)
    sudo mount -t cifs //twistedfate/motion /home/pi/motion -o user=,password=,dir_mode=0777,file_mode=0777

or in /etc/fstab
    
    //othon/motion /home/pi/motion cifs rw,nounix,user=,password=,dir_mode=0777,file_mode=0777 0 0

## Mount the AIMachine share (NFS)
    sudo mount twistedfate:/home/weseeyou/input /home/pi/motion 

or in /etc/fstab

    twistedfate:/home/weseeyou/input /home/pi/motion nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
    
# Camera installation
For complete installation instructions see [the official documentation](http://arducam.com/docs/cameras-for-raspberry-pi)

## Enable the Camera
First enable the camera via the raspi-config
        
    sudo raspi-config

(Optional) Early verions needed these additional steps to load the proper modules

    sudo modprobe i2c-dev

    sudo vi /boot/config.txt

add
    
    dtparam=i2c_vc=on

# Motion Detection Installation
## Install the motion software 

    sudo apt-get install motion

## Motion Configuration: image location
Configure the image storage location.
Note: make sure this directory is writable for the motion service user.

/etc/motion/motion.conf

    target_dir /home/pi/motion

## Motion Configuration: enable the daemon
Enable the motion daemon if you want to have it startup during system start.

/etc/default/motion
    
    start_motion_daemon=yes

## Motion Configuration: additional settings
/etc/motion/motion.conf

    stream_quality 75
    stream_localhost off
    output_pictures center
    ffmpeg_output_movies off
    framerate 2
    rotate 180

    ... 

    minimum_frame_time 1
    quality 100
    width 1080
    height 864
also change
    
    text_right %Y-%m-%d\n%T-%q
into 

    text_right
## Motion Configuration: make all filenames globally unique
Add a prefix to the image files to distinquish the result if you have multiple 'eyes'. 

Change

    picture_filename %v-%Y%m%d%H%M%S-%q

into 

    picture_filename W1%v-%Y%m%d%H%M%S-%q

# Motion service management
Also enable the motion service via the systemctl to make sure the service will start up during system start.

    sudo systemctl enable|disable motion
    sudo service motion start|stop|restart|status

## Check for output
Connect to the eye via http on port 8081 to verify that the camera and motion are working

    http://localhost:8081
or

    http://hostname:8081

For other motion.conf entries check [the official motion configuration pages](https://motion-project.github.io/motion_config.html)

## Troubleshooting
For troubleshooting check log file 
    
    /var/log/motion/motion.log

## Autofocus tool (OPTIONAL only for camera's with autofocus capability)
Install the autofocus tool. (note: make sure the motion service is stopped when executing the script AutoFocus.py). 

First install python-opencv

    sudo apt-get install python-opencv

Run 

    sudo python AutoFocus.py

# AutoFocus
To autofocus the raspberry camera make sure the motion service
is stopped and execute the following:

    cd /home/pi/weseeyou/WeSeeYou/AutoFocus
    sudo python AutoFocus.py

To check if the camera is focused properly check out the test.jpg in the AutoFocus directory

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - december 2020</i></small>
