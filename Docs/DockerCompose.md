# WeSeeYouDocker
The repository for the docker compose files for the weseeyou project.

Use these docker compose files to start up the application by starting up the docker images.

The weseeyou application suite: 
* aimachine for direct processing of the images received through the eye components.
* the research component for researching a template image.
* the ui for monitoring and starting of the research process.

## Login to the registry to get the images
    
    docker login registry.example.com -u <username> -p <token>

## Development
Pull the images from the registry.

    docker-compose -f docker-compose.dev.yml pull 

Start the application.

    docker-compose -f docker-compose.dev.yml up -d

Stop the application

    docker-compose -f docker-compose.prod.yml stop
    
## Production
Pull the images from the registry.

    docker-compose -f docker-compose.prod.yml pull 

Start the application

    docker-compose -f docker-compose.prod.yml up -d

Stop the application

    sudo docker-compose -f docker-compose.prod.yml stop

## Docker commands
### Remove images (all)
    docker system prune -a
### List images
    docker ps -a
### Read the log of a running container
    docker logs <container_name>

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>