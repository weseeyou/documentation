# Configure a raspberry pi gitlab runner for ARM images
https://www.devils-heaven.com/gitlab-runner-finally-use-your-raspberry-pi/

## Install Gitlab Runner
    
    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
    sudo apt-get install gitlab-runner

## Get Gitlab Runner Token
Open your project on gitlab.com.
Go to Settings > CI/CD > Runners.
Search for “Set up a specific Runner manually”.
Copy the Registration Token.

## The tokens
EYE Project Token: CYHsuwaswHdwWyRbyTVY
DRONE Project Token: BJWGUgEsDJ65deHEVDLn

## Register the runner
### EYE

    sudo gitlab-runner register -n \
        --url https://gitlab.com/ \
        --registration-token CYHsuwaswHdwWyRbyTVY \
        --executor docker \
        --description "Drone004 runner" \
        --docker-image "docker:stable" \
        --docker-privileged

### DRONE

    sudo gitlab-runner register -n \
        --url https://gitlab.com/ \
        --registration-token BJWGUgEsDJ65deHEVDLn \
        --executor docker \
        --description "Drone004 runner" \
        --docker-image "docker:stable" \
        --docker-privileged

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>