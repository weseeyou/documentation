# AIMachine in research mode
## General information
For research purposes an additional service module is created.

* The research server 
* The research process app
* The weseeyouui frontend app https://gitlab.com/spawnwvinull/weseeyouui that provides a research webpage on the /research address.

## The services
After all python packages are successfully installed we can start the services. The AIMachine research module has two services:
1.  the flask service to serve the api requests aka the <b>researchserver</b>

Start this service with:
    
    cd ~/weseeyou/AIMachine
    python Researchwsgi.py -c Config/Research/ResearchServer.json

2.  the service that listens for incoming images and sends them into the research process pipeline aka <b>research process app</b>

Start this service with:

    cd ~/weseeyou/AIMachine
    python ResearchProcessApp.py -c Config/Research/ResearchProcesApp.json

Browse to <url>/research
- then choose a file to transform
- then choose a file to use as style
- click submit

Check the logs and the research output directory for the results.

## Test enviroment
For the test puposes just start the services with the <b>test</b> configuration.

    cd ~/weseeyou/AIMachine
    python Reseachwsgi.py -c Config/Research/ResearchServer.TEST.json
    python ResearchProcessorApp.py -c Config/ResearchProcessApp.TEST.json

Please note that for CI integration purposes a [TEST CI](../AIMachine/Config/Research/ResearchServer.TEST.CI.json) version of the configuration is available. 

Start the website in the web project with for example yarn start.

Browse to the research page and submit source and style image. Output will be generated in the directory Research/output/<filename_without_extension>

##  Install the services (Ubuntu)
The two service can also be integrated in the system and installed as a native service via the following:

Install the services

    sudo cp ~/weseeyou/AIMachine/Install/researchserver.service /lib/systemd/system
    sudo cp ~/weseeyou/AIMachine/Install/researchprocessimages.service /lib/systemd/system
    sudo chmod 775 /lib/systemd/system/researchserver.service
    sudo chmod 775 /lib/systemd/system/researchprocessimages.service

Enable the services

    sudo systemctl enable researchserver.service
    sudo systemctl enable researchprocessimages.service

Start the services:
- aiserver

        sudo systemctl start researchserver.service 
        or 
        sudo service researchserver start etc
 
- processimages

        sudo systemctl start researchprocessimages.service 
        or   
        sudo service researchprocessimages start etc

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>