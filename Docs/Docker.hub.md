# The WeSeeYou machine learning art project
This a dockerized machine learning art project based on the neural style transfer principles described by Leon A. Gatys, Alexander S. Ecker, and Matthias Bethge in their paper ['A Neural Algorithm of Artistic style'](https://arxiv.org/abs/1508.06576) first published in 2015.

See the official project documentation [here](https://gitlab.com/weseeyou/documentation/-/blob/4ec0aca8d7b97c68a3ab086d2a4e708f543b9b77/Readme.md).
## Base Image Tags
-The images are based on the [tensorflow images](https://hub.docker.com/r/tensorflow/tensorflow) on this docker hub. Currently we are using [tag 2.2.1-gpu](https://hub.docker.com/layers/tensorflow/tensorflow/2.2.1-gpu/images/sha256-784f3c585ce4417aa159f7e5bf0476c045c8018af8bb3673d61b788208953eaf?context=explore)
- `aimachine-*` is the image for image style transfer. It has both the production as the research components on board. 
- `ui-*` is the backend management interface. Via this ui an overview of the available styles, created images and connected drones is available.
- `drone-*` is the image for hooking up simple display enabled devices for displaying the output of the aimachine.
- Use `latest` for production purposes and `latest-dev` for development purposes.

## Run containers
To run a container you could use the following:
    
    $ docker run -it --rm spawnwvinull/weseeyou:aimachine-latest /bin/bash

The images are based on the tensorflow-gpu enabled version which means the the `--gpus all` flag is available.

    $ docker run -it --rm --gpus all spawnwvinull/weseeyou:aimachine-latest /bin/bash

## AIMachine volumes
Inside the image there is an input and output volume available that can be used externally with a mounted volume.

    -v 'your_path'/input:/app/input
    -v 'your_path'/output:/app/output

And optionally you can provide an external log path for the log files.

    -v 'your_path'/logs:/app/logs

## AIMachine research
If you want to use the application in research mode to analyse style impact you can specify an alternative start command

    cmd ./startResearch.sh

You can also specify volumes to get direct access to the research results.

    -v 'your_path'/output:/app/Research/output
    -v 'your_path'/output:/app/output

And optionally you can provide an external log path for the log files.

    -v 'your_path'/logs:/app/logs

## Docker compose
You can use docker-compose to orchstrate the applications. A typical setup could start an aimachine and an aimachine in research mode together with a ui component. Keep in mind that providing a GPU to a container in a docker-compose file is done via the environment variable syntax.

    environment:
          - NVIDIA_VISIBLE_DEVICES=1

Author: Jeroen Vesseur

Website: http://work.wvinull.com/

License: [GNU General Public License](https://gitlab.com/weseeyou/documentation/-/blob/4ec0aca8d7b97c68a3ab086d2a4e708f543b9b77/LICENSE)

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>