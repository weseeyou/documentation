# Maintenance Overview Weseeyou

| Name           | Description           | Date              | Solution        | Effort         |
| -----          |------                 |-------------      |----             |-------------   |
| Drone001       | Upgrade to use Docker | December 2020     | Docker          | Medium         |
| Drone002       | Upgrade to use Docker | December 2020     | Docker          | Medium         |
| Drone007       | New Installation      | January 2021      | New Install     | Medium         |
| Drone008       | New Installation      | January 2021      | New Install     | Medium         |

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>