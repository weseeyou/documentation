# Ubuntu server 20.04 
## After the default installation
Prerequisite: I assume you know how to setup a new Ubuntu server with:
- a solid network configuration
- SSH server enabled
- root access
- no desktop please just cli
- some storage
- an NVIDIA GPU installed (I develped this doc with a GTX 1060 installation)

# Setup desired file structure
	sudo mkdir -p /home/weseeyou
	sudo mkdir -p /home/weseeyou/logs
	sudo mkdir -p /home/weseeyou/input
	sudo mkdir -p /home/weseeyou/output
	sudo mkdir -p /home/weseeyou/research
	mkdir -p /home/spawn/dev

	sudo chmod 777 /home/weseeyou
	sudo chmod 777 /home/weseeyou/*
	
	sudo chown nobody:nogroup /home/weseeyou
	sudo chown nobody:nogroup /home/weseeyou/*

## Setup gitlab sshkey
<todo>

## Some things to get out of the way
Add spawn to sudo group

	sudo visudo

add 

	spawn	ALL=(ALL) NOPASSWD:ALL

## Disable Hibernation
On the host system disable the hybernation settings.

		sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target

## Disable nouveau driver (if needed)
If the nouveau nvidia driver is still present we need to disable it for NVIDIA drivers to work properly.

		sudo bash -c "echo blacklist nouveau > /etc/modprobe.d/blacklist-nvidia-nouveau.conf"
		sudo bash -c "echo options nouveau modeset=0 >> /etc/modprobe.d/blacklist-nvidia-nouveau.conf"
		sudo update-initramfs -u

and reboot
		
## Standard apt update
		update apt
		sudo apt-get update
		sudo apt-get upgrade

## install the compiler tools (if needed)
	
		sudo apt-get install build-essential cmake unzip pkg-config
		sudo apt-get install gcc-6 g++-6

- install xwindows and opengl libraries

		sudo apt-get install libxmu-dev libxi-dev libglu1-mesa libglu1-mesa-dev

- install image and video io libraries

		sudo apt-get install libjpeg-dev libpng-dev libtiff-dev
		sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
		sudo apt-get install libxvidcore-dev libx264-dev

- install optimization libraries

		sudo apt-get install libopenblas-dev libatlas-base-dev liblapack-dev gfortran

- HDF5 for working with large datasets
		
		sudo apt-get install libhdf5-serial-dev

- we also need our Python 3 development libraries including TK and GTK GUI

		sudo apt-get install python3-dev python3-tk python-imaging-tk
		sudo apt-get install libgtk-3-dev

# Then the NVIDIA drivers and CUDA 
## NVIDIA drivers
- add apt-get repository to get the NVIDIA drivers and software

		sudo add-apt-repository ppa:graphics-drivers/ppa
		sudo apt-get update

- nstall the NVIDIA driver
	
		sudo apt-get install nvidia-driver-418

- after a reboot check the driver test_tf_installation
	
		sudo nvidia-smi
	or run

		sudo watch nvidia-smi

## CUDA (deprecated)
When using docker this is no longer needed.
- get cuda 10.0
		
		mkdir ~/temp
		cd temp
		wget https://developer.nvidia.com/compute/cuda/10.0/Prod/local_installers/cuda_10.0.130_410.48_linux
		
- make it a run file

		mv cuda_10.0.130_410.48_linux cuda_10.0.130_410.48_linux.run
- make it executable and run

		chmod +x cuda_10.0.130_410.48_linux.run
		sudo ./cuda_10.0.130_410.48_linux.run --override

- update the profile to include the binaries to the path but also to add the LD_LIBRARY_PATH. Add the following at the end of ~/.bashrc
	
		export PATH=/usr/local/cuda-10.0/bin:$PATH
		export LD_LIBRARY_PATH=/usr/local/cuda-10.0/lib64

- activate the new profile
	
		source ~/.bashrc

- validate the cuda installation with 
		
		nvcc -V

- finally download and extract cudnn 7.6 for cuda 10 from the [Nvidia developers network](https://developer.nvidia.com/rdp/cudnn-download). Note: you need an account to login to NVIDIA. Once downloaded. 
	
	Extract the tar.

		tar -zxvf cudnn-10.0-linux-x64-v7.6.5.32.tgz

	then copy the content of dirs lib64 and include into the local CUDA installation folder.

		cd cuda
		sudo cp -P lib64/* /usr/local/cuda/lib64/
		sudo cp -P include/* /usr/local/cuda/include/

## Install the WeSeeYou software (Deprecated)
- Clone repository
	
		git clone https://gitlab.com/spawnwvinull/weseeyou.git

- Create virtual env

		cd weseeyou
		python3 -m venv env

- Activate
	
		source env/bin/activate

- Upgrade pip
	
		python -m pip install --upgrade pip

- Upgrade setuptools 

		pip install setuptools==41.0.0

- Install required packages
	
		pip install -r requirements.aimachine.txt

- Test tensorflow with
	
		python ~/weseeyou/misc/test_tf_installation.py
		
- Create motion input dir 
	
		mkdir /home/weseeyou/input
	and
		
		chmod 777 /home/weseeyou/input

- Install the style images into TransferStyle/styles/current

	TODO

- Start the server
- Test with postman [POST] twistedfate:5016/api/drones

- For the image processor install libsm6 etc

		sudo apt-get install libsm6 libxext6 libxrender1 libfontconfig1

## Install and configure NFS
- Install NFS server

		sudo apt install nfs-kernel-server

- If not done already create the motion shared input directory
	
		sudo chown nobody:nogroup /home/weseeyou/input
		sudo chmod 777 /home/weseeyou/input

- Setup the export
		
		sudo vi /etc/exports
		/home/weseeyou/input *(rw,sync,no_subtree_check)

- Export the shared directory

		sudo exportfs -a
		sudo systemctl restart nfs-kernel-server

- Firewall (optional)
	
		sudo ufw status
		sudo ufw allow from <clientip> to any port nfs
		sudo ufw status

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - december 2020</i></small>