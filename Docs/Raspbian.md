# Raspberry pi with Raspbian
For the raspberries I have chosen the [Raspbian Buster](https://www.raspberrypi.org/downloads/raspbian/) version. To write the image to a micro SD I used the [Raspberry Pi Imager for Windows](https://downloads.raspberrypi.org/imager/imager.exe). Once the OS is properly booted I usually do the following:

Current version: 2020 02, kernel version 4.19.

- properly set timezone and language
- set the pi password
- set the hostname
- enable ssh
- boot into cli and not into desktop
- expand the fs (optional)

        sudo raspi-config --expand-rootfs

- connect to the wireless access point

        sudo vi /etc/wpa_supplicant/wpa_supplicant.conf

        country=NL
        network={
                ssid="WeSeeYou24"
                psk="***********"
                key_mgmt=WPA-PSK
        }

- update the environment with:

        sudo apt-get update
        sudo apt-get upgrade
        sudo rpi-update (only if needed)

- install git

        sudo apt-get install git

## NTP Configuration 
Because this is a standalone installation, the raspberries cannot access a time synchronization server on the internet. For this purpose the current AI machine is (or should be) configured as an NTP server.

### Configure ntp
Make sure ntp is enabled 

        sudo timedatectl set-ntp yes

Configure the ntp server on the pi by entering the hostname of an available ntp server in the configuration file.
    
    sudo vi /etc/systemd/timesyncd.conf

enter the following

    NTP=twistedfate

And reboot the pi.

Check configuration with

        sudo timedatectl

Output should be something like this: 

                    Local time: Fri 2020-06-12 22:20:12 CEST
                Universal time: Fri 2020-06-12 20:20:12 UTC
                      RTC time: n/a
                     Time zone: Europe/Amsterdam (CEST, +0200)
     System clock synchronized: yes
                   NTP service: active
               RTC in local TZ: no

Check service with 

        sudo service systemd-timesyncd status

## Set the raspberry filesystem to read-only mode (EXPIRIMENTAL)
**of course after the installation of the weseeyou software components**

To prevent sd card corruption it could be helpfull to set the filesystem to read-only mode. Then whenever the power is disconnected fs corruption on the SD card is avoided.

I followed [this](https://medium.com/swlh/make-your-raspberry-pi-file-system-read-only-raspbian-buster-c558694de79) article by Andreas Shallwig to configure the eye raspberry into read-only mode.

The procedure is as follows:
1. Configure the operating system to write all temporary files to the “tmpfs” file system which resides in memory.
2. Configure additional services to also use the tempfs file system.    
3. Redirect all system log files to memory.
4. Add some optional scripts to toggle read-only mode on / off

## Configure the operating system
Remove the following packages.

        sudo apt-get remove --purge triggerhappy logrotate dphys-swapfile

And cleanup the packages with: 

        sudo apt-get autoremove --purge

Disable swap and filesystem check and set it to read-only.

        sudo vi /boot/cmdline.txt
Add the line: 

        fastboot noswap ro
and disable the auto filesystem repair option

        fsck.repair=no
and make sure it looks something like this:

        console=serial0,115200 console=tty1 root=PARTUUID=97709164-02 rootfstype=ext4 elevator=deadline fsck.repair=no rootwait fastboot noswap ro

### Replace the log manager
Remove syslog and replace it with the busybox in-memory logger.

        sudo apt-get install busybox-syslogd
        sudo apt-get remove --purge rsyslog

From now on read logs with:

        sudo logread

### Make the file-systems read-only and add the temporary storage
Edit /etc/fstab and add the ro flag to the block devices.

        proc                  /proc     proc    defaults             0     0
        PARTUUID=fb0d460e-01  /boot     vfat    defaults,ro          0     2
        PARTUUID=fb0d460e-02  /         ext4    defaults,noatime,ro  0     1

And add the entries for the temporary file system

        tmpfs        /tmp            tmpfs   nosuid,nodev         0       0
        tmpfs        /var/log        tmpfs   nosuid,nodev         0       0
        tmpfs        /var/tmp        tmpfs   nosuid,nodev         0       0 

### Move some system files to temp filesystem

        sudo rm -rf /var/lib/dhcp /var/lib/dhcpcd5 /var/spool /etc/resolv.conf
        sudo ln -s /tmp /var/lib/dhcp
        sudo ln -s /tmp /var/lib/dhcpcd5
        sudo ln -s /tmp /var/spool
        sudo touch /tmp/dhcpcd.resolv.conf
        sudo ln -s /tmp/dhcpcd.resolv.conf /etc/resolv.conf

### Update the systemd random seed

        sudo rm /var/lib/systemd/random-seed
        sudo ln -s /tmp/random-seed /var/lib/systemd/random-seed

Edit the service config file

        sudo vi /lib/systemd/system/systemd-random-seed.service
And add and ExecStartPre to the services section.

        ExecStartPre=/bin/echo "" >/tmp/random-seed

### Create the commands ro and rw to switch between modes
Add the following to /etc/bash.bashrc

        set_bash_prompt() {
                fs_mode=$(mount | sed -n -e "s/^\/dev\/.* on \/ .*(\(r[w|o]\).*/\1/p")
        PS1='\[\033[01;32m\]\u@\h${fs_mode:+($fs_mode)}\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
        }
        alias ro='sudo mount -o remount,ro / ; sudo mount -o remount,ro /boot'
        alias rw='sudo mount -o remount,rw / ; sudo mount -o remount,rw /boot'
        PROMPT_COMMAND=set_bash_prompt

And make sure the filesystem is set to ro when logging out.

        sudo vi /etc/bash.bash_logout

Add the lines:

        mount -o remount,ro /
        mount -o remount,ro /boot

pi@DRONE002:~ $ sudo dphys-swapfile swapoff
pi@DRONE002:~ $ sudo dphys-swapfile uninstall
pi@DRONE002:~ $ sudo update-rc.d dphys-swapfile remove
pi@DRONE002:~ $ sudo apt purge dphys-swapfile

### Disable swap
Case: drone002 showed reboot issues while deactivating swap. To completely disable swap use the following: 

        sudo dphys-swapfile swapoff
        sudo dphys-swapfile uninstall
        sudo update-rc.d dphys-swapfile remove
        sudo apt purge dphys-swapfile

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - may 2020</i></small>