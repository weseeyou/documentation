# Manual installation
If you want you can install the software manually. This document describes the necessary step.

## Get the source code
This code is based on python version == 3.6 See the online [installation instructions](https://www.python.org/downloads/) for python installation. 

All code (AIMachine, The Eyes(WeSeeYou) and Drone) is available through this single project stored on gitlab.com. To get the code clone the repository.
    
    git clone https://gitlab.com/spawnwvinull/weseeyou.git

This will get you the following structure:

    weseeyou
    |__AIMachine
    |   |__wsgi.py
    |   |__Server.py
    |   |__requirements.aimachine.txt
    |   |__Installation.md
    |   |__<package directories>
    |   |__<etc ...>
    |__WeSeeYou
    |   |__MotionDetection
    |   |__AutoFocus
    |   |__Installation.md
    |   |__<etc ...>
    |__Drone
    |   |__wsgi.py
    |   |__Server.py
    |   |__requirements.drone.txt
    |   |__Installation.md
    |   |__<package directories>
    |   |__<etc ...>
    |__Shared
    |__misc

- <b>Shared</b> contains some shared utilities
- <b>misc</b> contains some helpfull scripts

## Setup the virtual environment
For now (march 2020) the python code is installed and run in a virtual environment. Configure virtual env for the AI machine and for the Drones.

- First upgrade pip 

        python3 -m pip install --upgrade pip

- Then install virtual env (if needed)

        python3 -m pip install virtualenv

- Then create/setup the virtual environment
    
        cd ~/weseeyou
        python3 -m venv env
    
- and activate it with 
    
        source env/bin/activate

- After activation of the virtual enviroment, upgrade pip again but now for this virtual environment.
    
        python3 -m pip install --upgrade pip

- Also upgrade the setuptools 
    
        pip install setuptools==41.0.0

- Then within the virtual env install the packages with 

    ### AIMachine

        cd ~/weseeyou/AIMachine
        pip install -r requirements.aimachine.txt

    ### Drone
    
        cd ~/weseeyou/Drone
        pip install -r requirements.drone.txt

- Finally setup a symbolic link to the Shared folder (DEVELOPMENT ONLY)

        cd ~/weseeyou/Drone 
        mklink /D "Shared" "..\Shared"

        cd ~/weseeyou/AIMachine 
        mklink /D "Shared" "..\Shared"

- After this refer to the specific installation instructions of each component for proper installation and configuration.

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>