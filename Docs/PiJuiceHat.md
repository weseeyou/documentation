# Pijuice hat

## Command line interface

    pijuice_cli

## Power button

- Single brief press to power on.
- Long press of at least 10 seconds to halt (soft shutdown).
- Long press of at least 20 seconds to cut power (hard shutdown).

## LED1 - Charge status

### With Pi off

![](./Images/blue.png) Blue steady: Battery fully charged

![](./Images/green.png) Green blinking: Charge over 50%

![](./Images/blue.png) Blue blinking: Charging

![](./Images/red.png) Red blinking: Low battery

### With Pi on

![./Images/green.png) Green blinking: Power on - Battery over 50%

![](./Images/blue.png) Blue blinking: Charging

![](./Images/orange.png) Orange blinking: Low battery - level < 50%

![](./Images/red.png) Red blinking: Low battery - level < 15% or battery absent

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>