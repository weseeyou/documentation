# Intro
This readme describes the installation steps of docker that can support tensorflow images with gpu support and will also describe the installation steps for the AIMachine and the AIResearch component for the weseeyou project.

# Docker configuration AIMACHINE
Tensorflow has support for gpu enabled dockers for Nvidia GPU cards enabled for docker. In the docker images the cuda installation is provided and the host system only needs to have the correct Nvidia GPU card drivers installed. (among some other things : )

The official [documentation](https://www.tensorflow.org/install/docker) to docker and tensorflow.

## Install Docker
The current installation of the host server is an ubuntu 20.04 installation that comes with docker already installed.

If not available install with 

    sudo apt install docker
    sudo apt install docker.io
    sudo apt install docker-compose

## Add user to docker group 
    
    sudo groupadd docker
    sudo usermod -aG docker spawn 

## Install the Nvidia GPU drivers
Use the ubuntu-drivers tool to determine a proper GPU device driver.

Official [documentation here](https://linuxconfig.org/how-to-install-the-nvidia-drivers-on-ubuntu-20-04-focal-fossa-linux)

If not available yet install ubuntu-drivers tool

    sudo apt install ubuntu-drivers-common

Check your devices

    sudo ubuntu-drivers devices

Install the drivers necessary for the system with 

    sudo ubuntu-drivers autoinstall

or use

    sudo apt install nvidia-driver-450

and reboot the system.



## Install the nvidia docker container toolkit
Official [documentation here](https://github.com/NVIDIA/nvidia-docker)

First setup the proper apt repositories by configuring the correct PGP key and the apt source list.

    distribution=$(. /etc/os-release;echo $ID$VERSION_ID)

    curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -

    curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

    sudo apt update


Then install the correct docker packages

    sudo apt install nvidia-docker2

and optionally the nvidia container toolkit

    sudo apt install nvidia-container-toolkit

and optionally (deprecated)

    sudo apt install nvidia-cuda-toolkit

To get proper support for the GPU set the default runtime for docker in the daemon.json. It should look like this:

    {
        "default-runtime": "nvidia",
        "runtimes": {
            "nvidia": {
                "path": "nvidia-container-runtime",
                "runtimeArgs": []
            }
        }
    }

Restart the docker daemon

    sudo service docker restart

## Test the docker installation
You can test the docker and gpu installation by spinning up a docker image with the --gpus flag.

    docker run -it --rm --gpus all ubuntu nvidia-smi -L

Use something like this to check the number of gpus found.

    docker run --gpus all -it --name gputest tensorflow/tensorflow:latest-gpu bash

    sudo docker exec -it gputest /bin/bash

    $python3
    >>>import tensorflow as tf
    >>>print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

The output should give you at least 1 gpu found (if you have one of course : )

Or you can check the tensorflow version in the interactive session.
    
    $python3 -c 'import tensorflow as tf; print(tf.__version__)

## Docker Compose
The aimachine, the research server and the ui are orchestrated with docker-compose. 

The aimachine and the research server are actually the same image with a different startup command. The ui is a react website in a node docker image.

This is the current (2020-10-05) docker-compose content for development. Check out the [docker project](https://gitlab.com/weseeyou/docker) for the updated state/version. The key thing to take note of now is the environment entry NVIDIA_VISIBLE_DEVICES=1. This is the counterpart of the option --gpu all in the earlier docker run statements.

    version: "3.3"
    services:
    # the ai machine (the main application)
    aimachine:
        image: "registry.gitlab.com/spawnwvinull/weseeyou:latest-dev"
        environment:
        - NVIDIA_VISIBLE_DEVICES=1 # which gpu do you want to use for this
        ports:
        - "5016:5016"
        volumes:
        - "/home/weseeyou/input:/app/input"
        - "/home/weseeyou/output:/app/output"
        - "/home/weseeyou/logs:/app/logs"
    # the research component
    aimachineresearch:
        image: "registry.gitlab.com/spawnwvinull/weseeyou:latest-dev"
        command: [./startResearch.sh]
        environment:
        - NVIDIA_VISIBLE_DEVICES=0 # which gpu do you want to use for this
        ports:
        - "5024:5024"
        volumes:
        - "/home/weseeyou/research:/app/Research/output"
        - "/home/weseeyou/logs:/app/logs"
    # the web page to view results and start research
    aiweb:
        image: "registry.gitlab.com/spawnwvinull/weseeyouui:latest-dev"
        ports:
        - "80:3000"
        depends_on:
        - "aimachine"
        - "aimachineresearch"


## Monitor gpu usage
On the host you can monitor gpu usage with the nvidi-smi tool.

Install if not available yet.

    sudo apt install nvidia-smi

Run with watch

    watch nvidia-smi

When processes start within the docker images that are using tensorflow with gpu you should see the memory usage here.



## Some interesting docker info
### Docker images gitlab login
    
    docker login registry.gitlab.com

### Run a tensorflow image and start python (interactive)
    docker run -it --rm tensorflow/tensorflow:latest-gpu python

    -it:  interactive terminal
    --rm: remove anonymous volumes

### Run a tensorflow image and start a bash shell (interactive) With access to the gpu

    docker run --gpus all -it tensorflow/tensorflow:latest-gpu bash

### Build a docker image
    sudo docker build --no-cache -t weseeyou .

    --no-cache: always pull the image that is defined in the dockerfile

### Run the weseeyou images with gpus and port redirection
    sudo docker run --gpus all -it -p 5016:5016 --name aimachine weseeyou

    --name: the name of the image that is spawned

### Run bash within the weseeyou image
    sudo docker run --gpus all -it -p 5016:5016 --name aimachine weseeyou bash

### Stop a running image
    sudo docker stop aimachine 

### Remove a stopped image
    sudo docker rm aimachine

### Start an image with mounts
    sudo docker run --gpus all \
    -p 5016:5016 \
    --name aimachine \
    --mount type=bind,source=/home/weseeyou/input,target=/app/input \
    --mount type=bind,source=/home/weseeyou/output,target=/app/output \
    weseeyou

### Docker compose pull latest images
    sudo docker-compose -f docker-compose.yml pull

### Docker compose Production
    
    sudo docker-compose -f docker-compose.atelier.yml up -d

    or 
    
    sudo docker-compose -f docker-compose.atelier.yml start

### Docker compose Development
    sudo docker-compose -f docker-compose.dev.yml up -d

# Raspberry pi 
Install docker on a raspbeery pi

   curl -sSL https://get.docker.com | sh

# Docker configuration DRONE
The drone docker container needs to run in privileged mode because it needs to connect to /dev/fb0 (the display frame buffer) to send images to.

    docker run -it --rm --network host --privileged registry.gitlab.com/weseeyou/drone:latest-dev /bin/sh

# Docker configuration EYE (canceled)
The eye needs to run in privileged mode because it needs to connect to the camera on /dev/video0.

    sudo docker run -it --rm --network host --privileged registry.gitlab.com/weseeyou/eye:latest-dev /bin/sh

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>