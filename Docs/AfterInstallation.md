# After the software installation and component setups
Once all componenents are properly installed and tested. Follow this procedure to start up the system.

## 1 Start up the router
The router needs to be started first because this device will provide communication capabilities to all components.

## 2 Start up a management machine (Optional)
Once the router is started you can start up an arbitrary system with ssh client capabilities to start monitoring the network and the proper functioning of the components. In my case I start up some wifi enabled windows 10 laptop with:
- visual studio code
- putty
- postman
- winscp

## 2 Start up the AIMachine
After the router is started properly the first real "WeSeeYou" component will be the AIMachine. This machine should be connected to the router via ethernet and not wifi.

Some useful postman api calls to make sure the services are started properly:
    
        # get the first available drone
        [GET] twistedfate:5016/api/drone

        # get the registered drones
        [GET] twistedfate:5016/api/drones   

        # check all known drones
        [POST] twistedfate:5016/api/drones/check  

        # get a subset of the generated images 
        # with the name of the drone they are send to 
        [GET] http://twistedfate:5016/api/images/stream/subset

        # get the style images 
        [GET] http://twistedfate:5016/api/styles



## 2 Start up the eye and (if needed Focus camera)
The the eye can be started and if necessary the camera can be focused. See [the Autofocus section](./Eye-Installation.md#Autofocus).

Keep in mind that the communication between the eye and the aimachine is provided via NFS.

## 3 Start up the drones
Connect the drones to their display devices and start them up. The first thing they will do is register with the AIMachine.

Some useful postman api calls to make sure everything is connected properly:
        
        # check if the drone is alive and if the service is up
        [GET] http://<drone_name>:5018/api/drone/checkservice

## 4 Sit back and enjoy the show
Now invite some friends and start the debate on new technology, mass surveillance, privacy, art and enjoy the images and a couple of beers.

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>