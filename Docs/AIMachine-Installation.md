# AIMachine installation instructions
## General information
The AI Machine component provides: 
    
- an API (Flask) for the drones to register themselves on.
- an NFS or CIFS share (depending on the operating system of course) where the eyes can store their images.
- a listener on the filesystem for incoming input images
- the AI component to recognize faces in the images. Based on the object detection implementation of [Zihao Zhang](https://github.com/zzh8829/yolov3-tf2)
- the AI component to transform the images. This process is an implementation of the paper [A Neural Algorithm of Artistic Style](https://arxiv.org/abs/1508.06576) by L.A. Gatys, A.S. Ecker and M. Bethge
- transformed output image storage. 
- capability to send transformed images to an available drone
- drone image management. keep track of which image has been sent to which drone
- an additional API for a client to:
    - retrieve this drone image management information 
    - retrieve the list of style images used in the style transfer process of transformation process
- logging to logfiles for monitoring

Note: this machane needs an NVIDIA GPU with properly installed drivers and a CUDA installation. Find more on that [here](./Ubuntu.md)

The We See You sequence

![A sequence visualisation](../Diagrams/TT_WeSeeYou.seq.png)

## Host file entries
Because there is no DNS server present for this installation. Put the correct entries in the [hosts file](./hosts.md#AIMachine-hosts-file).

## Get the source
    git clone https://gitlab.com/spawnwvinull/weseeyou.git

## The services
### Setup the virtual environment
For now (march 2020) the python code is installed and run in a virtual environment. Configure virtual env for the AI machine and for the Drones.

- First upgrade pip 

        python3 -m pip install --upgrade pip

- Then install virtual env (if needed)

        python3 -m pip install virtualenv

- Then create/setup the virtual environment
    
        cd ~/weseeyou
        python3 -m venv env
    
- and activate it with 
    
        source env/bin/activate

- After activation of the virtual enviroment, upgrade pip again but now for this virtual environment.
    
        python3 -m pip install --upgrade pip

- Also upgrade the setuptools 
    
        pip install setuptools==41.0.0

- Then within the virtual env install the packages with 

        cd ~/weseeyou/AIMachine
        pip install -r requirements.aimachine.txt

### Start the services
After all python packages are successfully installed we can start the services. The AIMachine has two services:
1.  the flask service to serve the api requests aka the <b>aiserver</b>

Start this service with:
    
    cd ~/weseeyou/AIMachine
    python AIwsgi.py -c Config/AIMachine.json

2.  the service that listens for incoming images and sends them into the process pipeline of Detection to Transformation to Distribution aka <b>processimages</b>

Start this service with:

    cd ~/weseeyou/AIMachine
    python AIProcessImagesApp.py -c Config/ProcessImagesApp.json

## The CIFS share (deprecated)
On a windows machine create a share called 'input' and make sure the raspberry pi user that runs the motion service on 'The Eye' has read and write access to it.

## The NFS share
Prerequisite: make sure the network is up and running and all machines can access each other based on ip or hostnames.

- Install NFS server

        sudo apt install nfs-kernel-server

- If not done already create the shared input directory

    	mkdir /home/weseeyou/input
        sudo chown nobody:nogroup /home/weseeyou/input
        sudo chmod 777 /home/weseeyou/input

- Setup the export in /etc/exports

    	/home/weseeyou/input *(rw,sync,no_subtree_check)

- Export the shared directory to enable client access
    
    	sudo exportfs -a
    	sudo systemctl restart nfs-kernel-server

- Firewall configuration (optional)

    	sudo ufw status
    	sudo ufw allow from <clientip> to any port nfs
    	sudo ufw status

## Scheduled reboot
If necessary the server can be rebooted on a regular basis eg. every sunday night at 23:00. Enter the following in the crontab of root.

        sudo crontab -e

        0 23 * * 0 /sbin/shutdown -r
        
## The configuration
Both processes are configured via the Config/*.json files.

<b>AIMachine.json</b>
    
    {
        "APPLICATION_PORT": "5016",
        "WORKING_DIR": "ImagesFromWSY",
        "INPUT_DIR": "/home/weseeyou/input",
        "BASE_DIR": "/home/spawn/weseeyou/AIMachine",
        "LOG_DIR": "logs",
        "LOG_FILE": "aimachine.log",
        "LOG_LEVEL": "INFO",
        "HOST_ADDRESS": "twistedfate",
        "DRONE_CHECK_SERVICE_ENDPOINT": "/api/drone/checkservice",
        "ENVIRONMENT": "PRODUCTION",
        "PATH_TO_STYLES": "TransferStyle/styles/current",
        "STYLE_OVERVIEW_CSV": "style_overview.csv"
    }

Property | Description | Example
----- |------ |-----
APPLICATION_PORT | The application port for the Flask Service | 5016
WORKING_DIR | The location where the service puts the files for ProcessImages service to listen | "ImagesFromWSY" 
INPUT_DIR | The location to expect the motion detection images are stored | "/home/weseeyou/input"
BASE_DIR | The application base dir | "/home/spawn/weseeyou/AIMachine"
LOG_DIR | The application log directory | "logs"
LOG_FILE | The log file name | "aimachine.log"
LOG_LEVEL | The log level | INFO, DEBUG, ERROR
HOST_ADDRESS | Host address definition for the Flask service  | "twistedfate"
DRONE_CHECK_SERVICE_ENDPOINT | The drone endpoint to call to do  a service check on the drone | "/api/drone/checkservice"
ENVIRONMENT | The environment label | PRODUCTION, TEST, DEVELOPMENT
PATH_TO_STYLES | The path to the style images and style_overview.csv | "TransferStyle/styles/current"
STYLE_OVERVIEW_CSV | The name of the csv that contains the style image information like name, description but also interesting model layer number for the style transfer process | "style_overview.csv"

<b>ProcessImagesApp.json</b>

    {
        "WORKING_DIR": "ImagesFromWSY",
        "BASE_DIR": "/home/spawn/weseeyou/AIMachine",
        "LOG_DIR": "logs",
        "LOG_FILE": "processimage.log",
        "LOG_LEVEL": "INFO",
        "AISERVER_BASEURL": "http://twistedfate:5016",
        "OUTPUT_PATH": "output",
        "STYLE_PATH": "TransferStyle/styles/current",
        "PYTHON_PATH": "/home/spawn/weseeyou/env/bin/python",
        "STYLE_OVERVIEW_CSV": "style_overview.csv"
    }

Property | Description | Example
----- |------ |-----
WORKING_DIR | The location where the service puts the files for ProcessImages service to listen | "ImagesFromWSY" 
BASE_DIR | The application base dir | "/home/spawn/weseeyou/AIMachine"
LOG_DIR | The application log directory | "logs"
LOG_FILE | The log file name | "processimage.log"
LOG_LEVEL | The log level | INFO, DEBUG, ERROR
AISERVER_BASEURL | The base url for the aiserver  | "http://twistedfate:5016"
OUTPUT_PATH | The output path for the transformed images  | "output"
PATH_TO_STYLES | The path to the style images and style_overview.csv | "TransferStyle/styles/current"
STYLE_OVERVIEW_CSV | The name of the csv that contains the style image information like name, description but also interesting model layer number for the style transfer process | "style_overview.csv"
PYTHON_PATH | The path to the python executable. (necessary for the child process forking) | "/home/spawn/weseeyou/env/bin/python"

## Test enviroment
For the test puposes just start the services with the <b>test</b> configuration.

    cd ~/weseeyou/AIMachine
    python AIwsgi.py -c Config/AIMachine.TEST.json
    python AIProcessImagesApp.py -c Config/ProcessImagesApp.TEST.json

Please note that for CI integration purposes a [TEST CI](../AIMachine/Config/AIMachine.TEST.CI.json) version of the configuration is available. 

##  Install the services (Ubuntu)
The two service can also be integrated in the system and installed as a native service via the following:

Install the services

    sudo cp ~/weseeyou/AIMachine/Install/* /lib/systemd/system
    sudo chmod 775 /lib/systemd/system/aiserver.service
    sudo chmod 775 /lib/systemd/system/processimages.service

Enable the services

    sudo systemctl enable aiserver.service
    sudo systemctl enable processimages.service

Start the services:
- aiserver

        sudo systemctl start aiserver.service 
        or 
        sudo service aiserver start etc
 
- processimages

        sudo systemctl start processimages.service 
        or   
        sudo service processimages start etc

## Logging and monitoring
The services and the process can be monitored in a variety of ways.
- service status commands

        sudo service aiserver status
        sudo service processimages status
- via the default syslog of the machine

        tail -f /var/log/syslog
- via the log files. All process log into the configured log directory (default ~/weseeyou/AIMachine/logs). There you will find:

        aimachine.log
        processimage.log
        transferstyle.log
## NTP Server
Because this is a standalone installation the AI machine is configured as an NTP server that the drones use to synchronize their time with.

- disable the built-in time sync service

        sudo systemctl disable systemd-timesyncd
        
- first install the ntp server component

        sudo apt-get install ntp

- configure via

        sudo vi /etc/ntp.conf

- configure to broadcast the ntp information

        broadcast 10.66.6.255

- with the following two lines tell the ntp service to trust it's own clock

        server 127.127.1.0
        fudge 127.127.1.0 stratum 1

- verify that the service is running

        sudo service ntp status
- if needed enable the service via sytemctl (optional)

        sudo systemctl enable ntp

- helpful commands

        sudo timedatectl
        sudo timedatectl timesync-status
        sudo timedatectl show-timesync


# Addendum
Still have to talk about 2 caveats:

1. The object recognition model is a pre-trained yolov3 model. Because of the size (240 MB) the model weights are not under source control. When the <b>aiserver</b> service starts up for the first time and does not find the weight file in 

        ~/weseeyou/AIMachine/DetectFaces/data 
    it will try to download it from a publicly accessible google cloud drive. Checkout the function 
    
        setup_ODFModel 
    
    in 
    
        ~/weseeyou/AIMachine/TransferStyle/Models/ModleUtils.py
    Here you will find the hardcode google drive address.

        https://drive.google.com/uc?id=19OrRNp_5DKLrCYOWhjJWaIXI9PSzcZmP
    
2. The same goes for the style images. The style images should be located in 

        ~/weseeyou/AIMachine/TransferStyle/styles/current

    next to the file 

        style_overview.csv
    
    In the csv the style images are defined together with the layers I want to use for style transfer but the images themselves are not included in the source code.

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - january 2021</i></small>

