# The host file
In this network there is no DNS server available. In order for the hosts to find each other the following must be entered in the hosts file of the AIMachine. 

Also see the network overview: [Network overview details](NetworkDetails.md)

## AIMachine hosts file
    127.0.0.1       localhost
    10.66.6.3       twistedfate.weseeyou.com        twistedfate
    10.66.6.4       weseeyou.weseeyou.com           weseeyou
    10.66.6.5       drone001.weseeyou.com           drone001
    10.66.6.6       drone002.weseeyou.com           drone002
    10.66.6.8       drone003.weseeyou.com           drone003
    192.168.178.39  drone004.weseeyou.com           drone004
    10.66.6.9       drone005.weseeyou.com           drone005
    10.66.6.8       drone006.weseeyou.com           drone006
    10.66.6.12      drone007.weseeyou.com           drone007
    10.66.6.13      drone008.weseeyou.com           drone008
    10.66.6.?       drone011.weseeyou.com           drone011


## Drone hosts file
* replace y with the correct values
    
        127.0.0.1       localhost
        127.0.0.1       drone<Y>.weseeyou.com           drone<Y>
        10.66.6.3       twistedfate.weseeyou.com        twistedfate

## Eye hosts file
* replace y with the correct values
    
        127.0.0.1       localhost
        127.0.0.1       weseeyou<Y>.weseeyou.com        weseeyou<Y>
        10.66.6.3       twistedfate.weseeyou.com        twistedfate

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>