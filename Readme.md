# Project: We See You
Author: Jeroen Vesseur

## Summary
The We See You Project is an art installation under the flag of the [Twin Towers](http://work.wvinull.com/Work/WorkDetail?projectName=Twin%20Towers) series. It is a project in which I want to address the contemperary problem of systems having access to our personal and public data and then using that data to classify and describe us. The installation is capable of 'observing' us by taking pictures. These pictures are then transformed into new images, using the [neural style transfer](https://en.wikipedia.org/wiki/Neural_Style_Transfer) principle. Then the transformation results are projected/displayed back to us, the audience. 

These new images now give us a new view on: how data, that is collected on us, is interpreted and used to try to understand and classify us. In this case it is all about aesthetics but the more typical use of course is commercial or political.
    
The installation is comprised of four major components: 
- "The Router": I have designed this installation to be a self contained network that is **not** connected to any other network or to the internet. For this I have chosen a router with strong wifi capabilities that can operate without a WAN connection and has at least one ethernet connection. The hope is that this gives the installation a chance to get preserved into a museum archive for later generations to view.
- "We See You": the representation of the 'eye(s)', in the form of a painting, that are watching us and act as the main data collector.
- "The AI Machine": the 'mastermind' as a spider in the web that 
    1. receives all the data 'We See' 
    2. analyses it, in style transfer context this means feature extraction. I call this: uses it to draw conclusions
    3. in the end transforms or distorts it according to its own view of the world or style in this case. This machine will implement the Neural Style Transfer principles using Tensorflow.
- "The Drone": a low cost piece of hardware (raspbery pi) with display capabilities that is connected via the network to the AI Machine and only has one job, that is to receive the transformed images and display them back to the audience.

![Schematic overview](./Diagrams/TT_Schema-readme.png)

These documents describe the installation and configuration of these components in more detail.

Date: august 2019 [See License](LICENSE)

# The components 
## The router
The We See You installation is designed to be a self contained network that is not connected to any other network or the internet. For this installation I have chosen for a router with strong wifi capabilities that can operate without a WAN connection and has at least one ethernet connection for the AI machine.

### Network configuration 
See [Network Configuration](./Docs/NetworkDetails.md#network-configuration)

## The AI Machine
The AI machine component is a straight forward single machine Ubuntu installation with a single NVIDIA GPU and a python, tensorflow with CUDA installation.

### System requirements (march 2020)
    Ubuntu 18.04
    Nvidia GPU (eg: GTX 1660)
    CUDA drivers (version 10)
    Tensorflow GPU (version 2.0)

The AI machine actually does all the heavy lifting; 
- it receives the raw images from the 'We See You' component(s)
- it transforms the data with different sets of neural networks
- it keeps track of the drones and sends the transformed images to the drones
 
The neural networks for style transfer and object detection are based on the Tensorflow library (GPU version). Therefore an NVIDIA GPU and a proper CUDA installation is required for this machine. For communication with the drones and management it has a public restfull API that is build with python's flask.

Detailed Installation Instructions [here](./Docs/AIMachine-Installation.md#aimachine-installation-instructions)

## The drones
The drones are raspberry pi's with a Raspbian installation and a display device attached.

There can be multiple drones. For the drones I have chosen cheap and lightweight hardware, the raspberry pi. These pi's are connected via wifi to the isolated network and have a display device connected via the hdmi interface. The displays (currently) are cheap devices from the surplus stores. At the moment the installation consists of four drones (march 2020) ranging from raspberry pi zero to 3B+ to 4.

The sole purpose of the Drone is to register with the AI Machine and receive images and display them to the display device. This component never has more than one image on storage.

Detailed Installation Instructions [here](./Docs/Drone-Installation.md#drone-installation-instructions)

## The eyes
The eyes are also raspberry pi's with a Raspbian installation and a camera attached. 

There can be multiple eyes as long as the images provided are uniquely identified. For the eyes I have chosen to use the raspberry pi zero with wifi capabilities and a camera connected. For the camera I have chosen the [Arducam Auto Focus Pi Camera](https://www.arducam.com/product/5mp-ov5647-motorized-focus-camera-sensor-raspberry-pi/), which as the name suggests has autofocus capabilities. The motion dection software used is [Motion](https://motion-project.github.io/) which is available under the GPL version 2.

The eye sends aproximately two images per second (if movement is detected) to the AI Machine for processing.

Detailed Installation Instructions [here](./Docs/Eye-Installation.md#weseeyou-the-eyes-installation-instructions)

## Docker
As of oktober 2020 the components are available as a docker image.

- AIMachine: registry.gitlab.com/weseeyou/aimachine
- UI:        registry.gitlab.com/weseeyou/ui
- Drone:     registry.gitlab.com/weseeyou/drone

You can find detailed instructions for the docker installation and use of the images [here](./Docs/Docker.md#Docker-configuration)

More on how docker compose comes into play read more  [here](./Docs/DockerCompose.md)

## Manual installation
If you want you can install the software manually (however docker is preferred). See more information [here](./Docs/ManualInstallation.md#manual-installation)

## Research mode
You can use this system to research the effect of a style to an input image. Read more about it [here](./Docs/AIMachine-Research.md)

## Schematic overview
A schematic overview of these components
![Schematic overview](./Diagrams/TT_overview_small.png)

# Examples
An example set of the transformation results.
![Example Set](./Diagrams/TT_summary_small.png)

# Benchmarks
The current system (twistedfate) is equipped with a NVDIA GTX 1060 GPU with 6 GB RAM.

GPU            | width (px) | height (px) | dpi | average time
---            |---         |----         |---  |------
RTX 3080 (10GB)| 1680       | 1680        | 72  | Out of memory
RTX 3080 (10GB)| 1480       | 1480        | 72  | 04:37
GTX 1080 (11GB)| 1880       | 1880        | 72  | Out of memory
GTX 1080 (11GB)| 1680       | 1680        | 72  | 06:48
GTX 1080 (11GB)| 1480       | 1480        | 72  | 05:23
GTX 1080 (11GB)| 1280       | 1280        | 72  | 03:53
GTX 1080 (11GB)| 1080       | 1080        | 72  | 02:58
GTX 1080 (11GB)| 800        | 600         | 72  | 01:10
GTX 1080 (11GB)| 640        | 480         | 72  | 00:47
GTX 1060 (6GB) | 1480       | 1480        | 72  | Out of memory
GTX 1060 (6GB) | 1280       | 1280        | 72  | 06:12
GTX 1060 (6GB) | 1080       | 1080        | 72  | 04:34
GTX 1060 (6GB) | 800        | 600         | 72  | 01:55
GTX 1060 (6GB) | 640        | 480         | 72  | 01:12
GTX 1050 (4GB) | 1080       | 1080        | 72  | Out of memory
GTX 1050 (4GB) | 800        | 600         | 72  | 03:07
GTX 1050 (4GB) | 640        | 480         | 72  | 02:08

# Addendum
Find some information on how to deploy this in a studio or gallery for example [here](AfterInstallation.md#After-the-software-installation-and-component-setups)

Find some more information on the PiJuice Hat [here](./Docs/PiJuiceHat.md)
# References
A Neural Algorithm of Artistic Style  by L.A. Gatys, A.S. Ecker and M. Bethge https://arxiv.org/abs/1508.06576

Deeplearning courses from the Lazy Programmer. https://lazyprogrammer.me/deep-learning-courses/

The Lazy Programmer on Udemy https://www.udemy.com/user/lazy-programmer/

The yolov3 github repository from Zihao Zhang https://github.com/zzh8829/yolov3-tf2

How to make your Raspberry Pi file system read-only from Andreas Shallwig https://medium.com/@andreas.schallwig/how-to-make-your-raspberry-pi-file-system-read-only-raspbian-stretch-80c0f7be7353

Raspberry installation documentation https://www.raspberrypi.org/documentation/installation/

Arducam autofocus camera documentation https://www.arducam.com/product/raspberry-pi-camera-5mp-autofocus-motorized-focus-camera-b0176/

Python 3.6 documentation https://www.python.org/downloads/release/python-360/

React documentation https://reactjs.org/docs/getting-started.html

<small><i>Jeroen Vesseur - jeroen.vesseur@gmail.com - april 2021</i></small>